AmaysimCart
By: Srilan John Catalinio
Date: 2017-05-19

To run the project:

1.0 Compilation
---------------
Compile the project using Main.java as the Main class.


2.0 User Guide
---------------
  The program will initially load the main screen prompting user for the following:
    List Products
    Show Cart
    Exit

  2.1 List Products
    This screen allows users to view all products available.
	  To modify the list of poducts, see section 3.1.

    While on this screen, users can add products to their carts by inputting the product code.
    If users have promo codes, they can input it next to the product code separated by space.

  2.2 Show Cart
    This screen lists all the products users added to their cart. 
    Also, the list of product codes and freebies are displayed on this section.

    Promo Code discounts, freebies and other promos are calculated and displayed on this screen.
    	To modify the promo codes see section 3.2
 
3.0 Settings
---------------
  3.1 Product List
    Product List can be found on product.csv. 
    It is a CSV file with the following format:
      [product code],[product name],[price]
      
      product code (alpha numeric) - the main identifier of the product.
      product name (alpha numeric) - the descriptive or display name of the product.
      price (decimal) - product price.

  3.2 Promo Codes
    You can maintain promo codes by modifying the promo_code.csv
    It is a CSV file with the following format:
      [promo code],[percent discount];[amount discount];[freebie];[new price]

      promo code (alpha numeric) - the promo code that users can input along with the product code
      percent discount (decimal) - percent off when the user inputs the promo code
      amount discount (decimal) - amount reduced from the price
      freebie (alpha numeric) - the product code of the freebie
      new price (decimal) - overrides the amount of the product

  3.3 Promo Combination 
     This is a special case when users purchase certain combination of products, such as 3x ult_small.
     You can maintain this settings by editing pricing_combi_promo.
     It is a CSV file with the following format:
       
       [product code 1];[product code 2];...;[product code n],[percent discount];[amount discount];[freebie];[new price]
       
       product code - list of product codes to be fulfilled in order to satisfy the promo

       percent discount (decimal) - percent off when the user inputs the promo code
       amount discount (decimal) - amount reduced from the price
       freebie (alpha numeric) - the product code of the freebie
       new price (decimal) - overrides the amount of the product
		
	
	
