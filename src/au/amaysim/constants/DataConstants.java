package au.amaysim.constants;

public class DataConstants {

	public static final String TABLE_PRODUCT = "PRODUCT";
	public static final String TABLE_PROMO_CODE = "PROMO_CODE";
	public static final String TABLE_PRICING_COMBINATION = "PRICING_COMBINATION";
}
