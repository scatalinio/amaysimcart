package au.amaysim.mainUI;

import java.util.List;
import java.util.Scanner;

import au.amaysim.controller.PricingRuleController;
import au.amaysim.controller.ProductController;
import au.amaysim.domain.entity.CartItem;
import au.amaysim.domain.entity.Product;
import au.amaysim.domain.entity.ShoppingCart;

public class UIManager {
	/**
	 * This is not really a UI. It's just made for demo purposes.
	 * Bootstrap would have been nice if I were to decide the UI.
	 */
	
	/*
	 * Indicates where we are in this UI map.
	 * 100 - Main Options
	 * 200 - Product List
	 * 300 - My Cart
	 */
	private int state = 0;

	// Classic command line listener
    private Scanner scanner = new Scanner(System.in);
    
    private ShoppingCart instanceCart;
    
    private ProductController productController;
    
	public UIManager() {
		try {
			
			//instantiate cart	
			instanceCart = ShoppingCart.createCart(new PricingRuleController().initPricingRule());

			// Spring dependency injection would be great to inject controller instances.
			productController = new ProductController();
			generateMainOptions();
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void generateMainOptions() throws Exception {
		state = 100;
		/**
		 * It may look like a college homework. 
		 * But it's just a demo.
		 */
		System.out.println("**********************************");
		System.out.println("1 - list products");
		System.out.println("2 - my cart");
		System.out.println("0 - bye?");
		optionListener();
	}
	
	private void showCart() throws Exception {
		state = 300;
		System.out.println("+--------------------------+-------------+");
		System.out.println("|Item                      | Quantity    |");
		System.out.println("+--------------------------+-------------+");
		
		String productRow = "|%-26s|%-13s|%n";
		for(CartItem cartItem: instanceCart.items()) {
		    System.out.format(productRow, cartItem.getItem(), cartItem.getQuantity());
		}
		System.out.println("+--------------------------+-------------+");
		System.out.println(instanceCart.total());

		System.out.println("Checkout not yet implemented");
		System.out.println("Enter 'x' to go to menu.");
		optionListener();
	}
	

	
	private void listProducts() throws Exception {
		state = 200;
		List<Product> products = productController.retrieveProducts();

		System.out.println("+-----------------+--------------------------+-------------+");
		System.out.println("| Product Code    |  Product Name            |    Price    |");
		System.out.println("+-----------------+--------------------------+-------------+");
		
		String productRow = "| %-15s | %-24s |%-13s|%n";
		for(Product product : products) {
		    System.out.format(productRow, product.getProductCode(), product.getProductName(), product.getPrice().setScale(2).toPlainString());
		}
		System.out.println("+-----------------+--------------------------+-------------+");

		System.out.println("Enter 'x' to go to menu.");
		System.out.println("Enter product code to add to cart. If you have a promo code, input it next to the product code separated by a space.");
		optionListener();
	}
	
	private void optionListener() throws Exception {
		String input = scanner.nextLine();
		
		if(state == 100) {
			switch(input) {
			case "1":
				listProducts();
				break;
			case "2":
				showCart();
				break;
			case "0":
				System.exit(0);
				break;
			default:
				generateMainOptions();
				break;
			}
		} else if(state == 200) {
			switch(input) {
			case "x":
				generateMainOptions();
				break;
			default:
				processInput(input);
				optionListener();
				break;
			}
		} else if(state == 300) {
			switch(input) {
			case "x":
				generateMainOptions();
				break;
			default:
				showCart();
				break;
			}
		}
	}
	
	private void processInput(String input) throws Exception {
		String productCode = input.split(" ")[0];
		Product product = productController.getProductByCode(productCode);
		if(product != null) {
			if(input.split(" ").length > 1) {
				String promoCode = input.split(" ")[1];
				instanceCart.add(product, promoCode);
			} else {
				instanceCart.add(product);
			}
			System.out.println("Product Code " + product.getProductName() + " added to cart.");		
		} else {
			System.out.println("Product Code " + productCode + " not found");			
		}
		listProducts();
	}
}
