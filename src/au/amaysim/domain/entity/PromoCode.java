package au.amaysim.domain.entity;

import java.util.List;

import au.amaysim.domain.entity.interfaces.Promo;

public class PromoCode extends Promo{
	
	private String promoCode;

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	@Override
	public boolean isSatified(List<String> codes) {
		if(codes.equals(promoCode)) {
			return true;
		}
		return false;
	}
	
	
}
