package au.amaysim.domain.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.amaysim.controller.ProductController;
import au.amaysim.domain.entity.interfaces.Cart;

public class ShoppingCart implements Cart{

	private List<Product> products;
	private PricingRules pricingRules;
	private List<String> promoCodes;
	
	private ProductController productController;

	public ShoppingCart(PricingRules pricingRules) {
		this.pricingRules = pricingRules;
		products = new ArrayList<Product>();
		promoCodes = new ArrayList<String>();
		productController = new ProductController();
	}
	
	public static ShoppingCart createCart(PricingRules pricingRules) {
		return new ShoppingCart(pricingRules);
	}

	@Override
	public void add(Product item) {
		products.add(item);
	}

	@Override
	public void add(Product item, String promoCode) {
		products.add(item);
		
		/*
		 *  It is unclear whether the promo code will affect on the item where the code is applied with or 
		 *  it will apply to all cart items. But base on the test scenarios, it is applied to all items on the cart.
		 */		
		promoCodes.add(promoCode);
		
	}

	public BigDecimal total() {
		List<String> productCodes = new ArrayList<String>();
		BigDecimal totalPrice = BigDecimal.ZERO;
		for(Product product : products) {
			productCodes.add(product.getProductCode());
			totalPrice = totalPrice.add(product.getPrice());
		}
		for(PricingCombination pricingCombination : pricingRules.getPricingCombination()) {
			if(pricingCombination.isSatified(productCodes)) {
				// remove price from total price
				// And make computation for the new price
				BigDecimal newPrice = BigDecimal.ZERO;
				for(String codeCombi : pricingCombination.getCodeCombination()) {
					Product prodCombi = null;
					try {
						prodCombi = productController.getProductByCode(codeCombi);
						totalPrice = totalPrice.subtract(prodCombi.getPrice());						
						if(pricingCombination.getDiscount() != null) {
							BigDecimal discountedPrice = prodCombi.getPrice().subtract(
									prodCombi.getPrice().multiply(BigDecimal.valueOf(pricingCombination.getPercentDiscount())));
							newPrice = newPrice.add(discountedPrice);
						}							
						if(pricingCombination.getPriceAdjust() != null) {
							newPrice = newPrice.add(pricingCombination.getPriceAdjust());
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				if(pricingCombination.getPercentDiscount() > 0) {
					newPrice = newPrice.subtract(newPrice.multiply(BigDecimal.valueOf(pricingCombination.getPercentDiscount())));
				}
				totalPrice = totalPrice.add(newPrice);
			}
		}
		for(PromoCode promoCode : pricingRules.getPromoCode()) {
			if(promoCode.isSatified(productCodes)) {
				if(promoCode.getDiscount() != null) {
					totalPrice = totalPrice.subtract(promoCode.getDiscount());
				}
				if(promoCode.getPercentDiscount() > 0) {
					totalPrice = totalPrice.subtract(totalPrice.multiply(BigDecimal.valueOf(promoCode.getPercentDiscount())));
				}
			}
		}
		return totalPrice;
	}

	public List<CartItem> items() {
		List<CartItem> items = new ArrayList<CartItem>();

		List<String> productCodes = new ArrayList<String>();
		for(Product product : products) {
			productCodes.add(product.getProductCode());
		}
		Set<String> uniqueItem = new HashSet<String>(productCodes);
		for (String temp : uniqueItem) {
			Product uniqueProduct;
			try {
				uniqueProduct = productController.getProductByCode(temp);
				CartItem cartItem = new CartItem();
				cartItem.setItem(uniqueProduct.getProductName());
				cartItem.setQuantity(Collections.frequency(productCodes, temp));
				items.add(cartItem);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		for(PricingCombination pricingCombination : pricingRules.getPricingCombination()) {
			if(pricingCombination.isSatified(productCodes) && !pricingCombination.getFreebies().isEmpty()) {
				Product freebieProduct = null;
				try {
					freebieProduct = productController.getProductByCode(pricingCombination.getFreebies());
					CartItem cartItem = new CartItem();
					cartItem.setItem(freebieProduct.getProductName());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		for(PromoCode promoCode : pricingRules.getPromoCode()) {
			if(promoCode.isSatified(productCodes)) {
				CartItem cartItem = new CartItem();
				cartItem.setItem(promoCode.getPromoCode());
			}
		}
		return items;
	}
	
}
