package au.amaysim.domain.entity;

import java.util.List;

public class PricingRules {
	
	private List<PricingCombination> pricingCombination;
	private List<PromoCode> promoCode;
	
	public List<PricingCombination> getPricingCombination() {
		return pricingCombination;
	}
	public void setPricingCombination(List<PricingCombination> pricingCombination) {
		this.pricingCombination = pricingCombination;
	}
	public List<PromoCode> getPromoCode() {
		return promoCode;
	}
	public void setPromoCode(List<PromoCode> promoCode) {
		this.promoCode = promoCode;
	}
	
	
	
	
	
	
}
