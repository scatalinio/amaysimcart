package au.amaysim.domain.entity;

import java.util.List;

import au.amaysim.domain.entity.interfaces.Promo;

public class PricingCombination extends Promo {

	private List<String> codeCombination;

	public List<String> getCodeCombination() {
		return codeCombination;
	}

	public void setCodeCombination(List<String> codeCombination) {
		this.codeCombination = codeCombination;
	}


	@Override
	public boolean isSatified(List<String> codes) {
		if(codeCombination != null) {
			for(String code : codeCombination) {
				if(!codes.contains(code)) {
					return false;
				} else {
					codes.remove(code);
				}
			}
		}
		return true;
	}
	
	
}
