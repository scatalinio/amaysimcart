package au.amaysim.domain.entity.interfaces;

import java.math.BigDecimal;
import java.util.List;

import au.amaysim.domain.entity.Product;

public abstract class Promo {

	private double percentDiscount;
	private BigDecimal discount;
	private String freebies;
	private BigDecimal priceAdjust;
	
	public double getPercentDiscount() {
		return percentDiscount;
	}
	public void setPercentDiscount(double percentDiscount) {
		this.percentDiscount = percentDiscount;
	}
	public BigDecimal getDiscount() {
		return discount;
	}
	public void setDiscount(BigDecimal discount) {
		this.discount = discount;
	}
	
	public String getFreebies() {
		return freebies;
	}
	public void setFreebies(String freebies) {
		this.freebies = freebies;
	}
	
	public BigDecimal getPriceAdjust() {
		return priceAdjust;
	}
	public void setPriceAdjust(BigDecimal priceAdjust) {
		this.priceAdjust = priceAdjust;
	}
	public abstract boolean isSatified(List<String> codes);
	
}
