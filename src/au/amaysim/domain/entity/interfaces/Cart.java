package au.amaysim.domain.entity.interfaces;

import au.amaysim.domain.entity.Product;

public interface Cart {

	
	public abstract void add(Product item);
	
	public abstract void add(Product item, String promoCode);
	
	
	
	
}
