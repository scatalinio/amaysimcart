package au.amaysim.dao.interfaces;

import java.util.List;

public interface DAOInterface<T> {

	public void save(T object) throws Exception ;
	public void delete(T object) throws Exception ;
	public T load(T object) throws Exception ;
	public T load(String id) throws Exception ;
	public List<T> list(String query) throws Exception ;
}
