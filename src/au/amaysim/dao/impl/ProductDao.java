package au.amaysim.dao.impl;

import java.util.List;

import au.amaysim.constants.DataConstants;
import au.amaysim.dao.interfaces.DAOInterface;
import au.amaysim.domain.entity.Product;

public class ProductDao extends CommonDao implements DAOInterface<Product> {

	public void save(Product object)  throws Exception {
		
	}
	public void delete(Product object)  throws Exception {
		
	}
	public Product load(Product object)  throws Exception {
		return null;
	}
	
	public Product load(String productCode)  throws Exception {
		/*
		 * Substitute for a select statement.
		 * Retrieve list of products and compare product codes from productCode param.
		 * Better than nothing.
		 */
		List<Product> producs = list(null);
		for (Product product : producs) {
			if(product.getProductCode().equals(productCode)) {
				return product;
			}
		}
		return null;
	}
	

	public List<Product> list(String query) throws Exception {
		DataManager dm = new DataManager();
		List<Product> products = dataToProduct(dm.retrieveData(DataConstants.TABLE_PRODUCT));		
		return products;
	}
}
