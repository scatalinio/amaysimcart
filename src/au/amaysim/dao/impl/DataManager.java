package au.amaysim.dao.impl;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import au.amaysim.constants.DataConstants;

public class DataManager {

	/**
	 * Instead of retrieving from the database, retrieve from CSV?
	 * All paths will be hard coded for now.
	 * 
	 * Hibernate framework would have been easier but rules indicates that "No Third-party frameworks".
	 * 
	 * Messy, but does the job.
	 * @throws Exception 
	 */


	public List<String[]> retrieveData(String table) throws Exception {
		String csvFile = "";
		List<String[]> retData = new ArrayList<String[]>();
		switch (table) {
			case DataConstants.TABLE_PRODUCT:
				csvFile = System.getProperty("user.dir") + "/product.csv";
				break;
			case DataConstants.TABLE_PRICING_COMBINATION:
				csvFile = System.getProperty("user.dir") + "/pricing_combi_promo.csv";
				break;
			case DataConstants.TABLE_PROMO_CODE:
				csvFile = System.getProperty("user.dir") + "/promo_code.csv";
				break;
	
			default:
				break;
		}


		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(csvFile));			
			while ((line = br.readLine()) != null) {
				String[] data = line.split(cvsSplitBy,  -1);
				retData.add(data);
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			throw new Exception("CSV file for " + table  + " not found");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return retData;
	}
}