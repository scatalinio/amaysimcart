package au.amaysim.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.amaysim.domain.entity.PricingCombination;
import au.amaysim.domain.entity.Product;
import au.amaysim.domain.entity.PromoCode;

public abstract class CommonDao {

	
	protected List<Product> dataToProduct(List<String[]> data) {
		List<Product> products = new ArrayList<Product>();
		for(String[] arrayData : data) {
			Product product = new Product();
			product.setProductName(arrayData[1]);
			product.setProductCode(arrayData[0]);
			/*
			 * Assuming that all data are correct and data are retrieve from RDBMS.
			 * 
			 */
			product.setPrice(new BigDecimal(arrayData[2]));
			products.add(product);
		}
		return products;
	}
	

	protected List<PromoCode> dataToPromoCode(List<String[]> data) {
		List<PromoCode> promoCodes = new ArrayList<PromoCode>();
		for(String[] arrayData : data) {
			PromoCode promoCode = new PromoCode();
			promoCode.setPromoCode(arrayData[0]);
			
			String[] promoData = arrayData[1].split(";",  -1);
			if(!promoData[0].isEmpty())
				promoCode.setPercentDiscount(Double.parseDouble(promoData[0]));
			if(!promoData[1].isEmpty())
				promoCode.setDiscount(new BigDecimal(promoData[1]));
			if(!promoData[2].isEmpty())
				promoCode.setFreebies(promoData[2]);
			if(!promoData[3].isEmpty())
				promoCode.setPriceAdjust(new BigDecimal(promoData[3]));
			promoCodes.add(promoCode);
		}
		return promoCodes;
	}
	
	

	protected List<PricingCombination> dataToPricingCombination(List<String[]> data) {
		List<PricingCombination> pricingCombinations = new ArrayList<PricingCombination>();
		for(String[] arrayData : data) {
			PricingCombination pricingCombination = new PricingCombination();
			pricingCombination.setCodeCombination(Arrays.asList(arrayData[0].split(";",  -1)));
			String[] promoData = arrayData[1].split(";",  -1);
			if(!promoData[0].isEmpty())
				pricingCombination.setPercentDiscount(Double.parseDouble(promoData[0]));
			if(!promoData[1].isEmpty())
				pricingCombination.setDiscount(new BigDecimal(promoData[1]));
			if(!promoData[2].isEmpty())
				pricingCombination.setFreebies(promoData[2]);
			if(!promoData[3].isEmpty())
				pricingCombination.setPriceAdjust(new BigDecimal(promoData[3]));
			pricingCombinations.add(pricingCombination);
		}
		return pricingCombinations;
	}
}
