package au.amaysim.dao.impl;

import java.util.List;

import au.amaysim.constants.DataConstants;
import au.amaysim.dao.interfaces.DAOInterface;
import au.amaysim.domain.entity.PricingCombination;
import au.amaysim.domain.entity.Product;
import au.amaysim.domain.entity.PromoCode;

public class PromoDao extends CommonDao {

	public List<PromoCode> retrievePromoCodes() throws Exception {
		DataManager dm = new DataManager();
		List<PromoCode> products = dataToPromoCode(dm.retrieveData(DataConstants.TABLE_PROMO_CODE));		
		return products;
	}

	public List<PricingCombination> retrievePricingCombinations() throws Exception {
		DataManager dm = new DataManager();
		List<PricingCombination> pricingCombinations = dataToPricingCombination(dm.retrieveData(DataConstants.TABLE_PRICING_COMBINATION));		
		return pricingCombinations;
	}
	
}
