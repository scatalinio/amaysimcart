package au.amaysim.controller;

import au.amaysim.dao.impl.PromoDao;
import au.amaysim.domain.entity.PricingRules;

public class PricingRuleController {

	public PricingRules initPricingRule() {
		PromoDao promoDao = new PromoDao();
		PricingRules pricingRule = new PricingRules();
		try {
			pricingRule.setPricingCombination(promoDao.retrievePricingCombinations());
			pricingRule.setPromoCode(promoDao.retrievePromoCodes());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pricingRule;
	}
}
