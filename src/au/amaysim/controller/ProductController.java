package au.amaysim.controller;

import java.util.List;

import au.amaysim.dao.impl.ProductDao;
import au.amaysim.domain.entity.Product;

public class ProductController {

	ProductDao productDao;
	
	public ProductController() {
		// Let's avoid singleton. Create a new instance.
		productDao = new ProductDao();
	}
	
	public List<Product> retrieveProducts() throws Exception {
		return productDao.list(null);
	}
	
	public Product getProductByCode(String productCode) throws Exception {
		return productDao.load(productCode);
	}
}
